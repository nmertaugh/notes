## Seeing which computers/machines are on the local network:

1. First get the IP and mask of the local network:

`$ ip address`

2. Look for a lone that looks something like:

> inet 192.168.1.158/24 brd 192.168.1.255 scope global dynamic noprefixroute wlp4s0

3. Use `nmap` and the IP address to see info on local machines:

`$ nmap 192.168.1.158/24`

## How to serve Rails app to other machines on network

1. Start the server and bind it to universal address:

`$ bundle exec rails s -b 0.0.0.0 -p 3000 `

2. On other machine navigate to local IP address of serving machine (see above). For example, `192.168.1.158:3000`

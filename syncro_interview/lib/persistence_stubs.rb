module PersistenceStubs
  def self.included(klass)
    klass.extend ClassMethods
    klass.extend SharedMethods
    klass.include InstanceMethods
    klass.include SharedMethods
  end

  module ClassMethods
    def belongs_to(association)
      attr_accessor association
    end

    def has_many(pluralized_association)
      singularized_association = pluralized_association.to_s.singularize

      define_method pluralized_association do
        instance_var = "@_#{pluralized_association}"
        instance_variable_value = instance_variable_get(instance_var)

        instance_variable_value or instance_variable_set(instance_var, QueryableArray.new)
      end
    end

    def has_one(association)
      attr_accessor association
    end

    def validates(association, **_)
      attr_accessor association
    end
  end

  module InstanceMethods
    def id
      @_id ||= rand(1000)
    end
  end

  module SharedMethods
    def method_missing(*args)
      self
    end
  end
end

class QueryableArray < Array
  def where(**conditions)
    select do |element|
      conditions.each_pair.map do |key, value|
        element.public_send(key) == value
      end.all?
    end
  end
end

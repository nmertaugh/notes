class Invoice < ApplicationRecord
  validates :total, presence: true
end

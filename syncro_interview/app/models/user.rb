class User < ApplicationRecord
  belongs_to :account
  has_many :invoices
  has_one :bank_account

  # ...

  validates :mobile_phone_number, presence: true

  # ...

  def active?
    if account.admin?
      true
    else
      account.membership.plan.expired?
    end
  end

  def pay_outstanding_invoices
    total_paid = invoices.where(paid: false).each.inject(0) do |invoice, total|
      total += invoice.total
      PaymentService.run(invoice: invoice, amount: invoice.total, bank_account: bank_account)
      invoice.update(paid: true)
      log_payment(invoice, total)
    end

    SmsGateway.text(
      phone_number: mobile_phone_number,
      message: "Your invoices are paid in full: $#{total_paid}"
    )
  end

  private

  def log_payment(invoice, amount_paid)
    LoggerService.log("Invoice #{invoice.id} paid in full: $#{amount_paid}")
  end
end

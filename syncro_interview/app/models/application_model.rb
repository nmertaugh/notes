require_relative '../../lib/persistence_stubs'

class ApplicationModel
  include PersistenceStubs
  include ActiveSupport
end

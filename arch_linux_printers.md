## Setting Up My Network Printer on Arch Linux

1. Download drivers for _Dell E514dw_ [here](http://www.dell.com/support/home/us/en/04/Drivers/DriversDetails?driverId=FV8XM) (these come in RPM and DEB formats)

1. Inside `~/Downloads`, use `rpmextract` cheat to extract RPM version for Arch
```sh
sudo pacman -S rpmextract
cd ~/Downloads
rpmextract.sh e514dwcupswrapper-3.2.0-1.i386.rpm
rpmextract.sh  e514dwlpr-3.2.0-1.i386.rpm
```

1. CUPS (printing system) uses `.ppd` files as drivers to talk to printers. We need to move one from the `usr` directory `rpmextract` created inside `~/Downloads` for us into the directory CUPS expects drivers to be in:
```sh
sudo cp ~/Downloads/opt/dell/Printers/E514dw/cupswrapper/dell-E514dw-cups-en.ppd /usr/share/ppd/cupsfilters/dell-E514dw-cups-en.ppd
```

1. Before we add our printer, we need to get its lpd address by which CUPS will contact it
```sh
sudo lpinfo -v #
# => network lpd://DELLB354D9/BINARY_P1
```

1. Empirically, `lpd://DELLB354D9/BINARY_P1` didn't work, so we need to replace `DELLB354D9` with the printer's local IP address. To get that, we first get the interface for our home machine:
```sh
ip addr
# => ...
# => 3: enp0s31f6: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
# => ...
```

1. Then we use a tool called arp-scan to get the local address of the printer:
```sh
sudo arp-scan --interface=enp0s31f6 --localnet -v
# => ...
# => 10.0.1.9	00:18:61:36:83:f7	Ooma, Inc.
# => 10.0.1.4	e8:ab:fa:17:ea:8e	Shenzhen Reecam Tech.Ltd.
# => 10.0.1.12	48:e2:44:b3:54:d9	(Unknown)
# => ^^^^^^^^^ --> guessing this one turned out to be correct
```

1. Now we are in a position to use the CUPS instructions on the [Arch Wiki](https://wiki.archlinux.org/index.php/CUPS#CLI_tools) to add our printer and set the default printer
```sh
sudo lpadmin -p DELL_E514DW -E -v 'lpd://10.0.1.12/BINARY_P1' -m dell-E514dw-cups-en.ppd
lpoptions -p DELL_E514DW
```

1. To check the stats of the printer, we can:
```sh
lpstat -s
# => system default destination: DELL_E514DW
# => device for DELL_E514DW: lpd://DELLB354D9/BINARY_P1
```

1. We can also go to http://localhost:631 to use the CUPS web interface to troubleshoot/config etc

1. For the scanner, I was able to use a brother driver available in the AUR:
```sh
yaourt -S brscan4
brsaneconfig4 -a name=E514dw model=MFC-L2700DW ip=10.0.1.12
# IP from above, will likely need a static IP instead
```
